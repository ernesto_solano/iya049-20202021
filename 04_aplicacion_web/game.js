//<img id ="bg-img" src="https://i.ibb.co/mRYZgjW/bg.gif" alt="background">
let points = 0;
const pageWidth = window.innerWidth;
const pageWidthHalf = pageWidth / 2;
const elementp1 = document.getElementById("player-1");
const elementp2 = document.getElementById("player-2");
const buttona = document.getElementById("buttona");
const buttonk = document.getElementById("buttonk");
const buttons = document.getElementById("buttons");
const buttonl = document.getElementById("buttonl");
const buttonresetgame = document.getElementById("buttonresetgame");
const domain = "http://localhost:8080"

function Player(name, hp, attack, defense, charged_attack) {
    this.name = name;
    this.hp = hp;
    this.attack = attack;
    this.defense = defense;
    this.charged_attack = charged_attack;
}
const throttle = (delay, fn) => {
    let inThrottle = false;
    return (args1, args2) => {
        if (inThrottle) {
            return;
        }

        inThrottle = true;
        fn(args1, args2);
        setTimeout(() => {
            inThrottle = false;
        }, delay);
    };
};

const move_image = (points, atacker) => {
    let resize = pageWidthHalf + points;

    elementp1.style.position = "absolute";
    elementp2.style.position = "absolute";

    elementp1.style.left = resize - elementp1.offsetWidth + "px";
    elementp2.style.left = resize + "px";
    console.log(resize, elementp1.offsetWidth);
    if (atacker != undefined)
        hit_animation(atacker);
    detect_collition(resize);
}

const hit_animation = (atacker) => {
    if (atacker.name === 'p1') {
        let initialPositionLeft = elementp1.getBoundingClientRect().left;
        elementp1.style.left = initialPositionLeft + pageWidthHalf / 5 + "px";

        setTimeout(() => { elementp1.style.left = initialPositionLeft + "px"; }, 350)


    }
    else if (atacker.name === 'p2') {
        let initialPositionLeft = elementp2.getBoundingClientRect().left;
        elementp2.style.left = initialPositionLeft - pageWidthHalf / 5 + "px";

        setTimeout(() => { elementp2.style.left = initialPositionLeft + "px"; }, 350)

    }
}
const victory = (player) => {
    var myAudio = document.getElementById("audioyou-win")
    myAudio.play();
    fetchScorePost(player).then(data => {
        fetchScore().then(data => window.alert(`
            VICTORIA DE ${player.name}\n 
            ==================================\n
            El marcador es el siguiente:\n
             p1: ${data["p1"]} victorias\n
             p2: ${data["p2"]} victorias\n
            ==================================
        `));
    });
    cleanGame();
}
const cleanGame = () => {
    var highestTimeoutId = setTimeout(";");
    for (var i = 0; i < highestTimeoutId; i++) {
        clearTimeout(i);
    }
    points = 0;
    move_image(points);
    buttona.style.backgroundColor = "";
    buttonk.style.backgroundColor = "";
    buttons.style.backgroundColor = "";
    buttonl.style.backgroundColor = "";
}

const fetchScore = async () => {

    const response = await fetch(`${domain}/score.json`, { method: "GET" });
    const score = await response.json();
    return score;
}
const fetchScorePost = async (player) => {
    const data = { "winner": player.name }
    const response = await fetch(`${domain}/score`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const score = await response.json();
    return score;
}

const fetchScoreClearPost = async (player) => {
    const data = { "clear": "score" }
    const response = await fetch(`${domain}/score/clear`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const score = await response.json();
    return score;
}

const clearScore = () => {
    if (window.confirm("Vas a borrar los marcadores, ¿Estás seguro?")) {
        fetchScoreClearPost().then(() => {
            window.alert("Se han borrado los marcadores");
            cleanGame();
        })
    }
}

const detect_collition = () => {
    var p1Position = elementp1.getBoundingClientRect();
    var p2Position = elementp2.getBoundingClientRect();
    console.log(p1Position.left, p2Position.right);
    if (p1Position.left < 0) {
        victory(player2);


    }
    else if (p2Position.right > pageWidth) {
        victory(player1)
    }
}



const attack_action1 = (player, enemy) => {

    if (player.defense === true) {
        console.log("no puedes atacar mientras defiendes");
    }
    else {
        if (enemy.defense === false) {
            enemy.hp--;
            if (player.charged_attack === true) {
                points = points + pageWidthHalf / 5;
                var myAudio = document.getElementById("ping");
                myAudio.play();
                player.charged_attack = false;
            }
            else {
                var myAudio = document.getElementById("punch");
                myAudio.play();
                points = points + pageWidthHalf / 10;
            }
            move_image(points, player);
        }
        else {
            console.log("defendido");
            if (player.charged_attack === true) {
                points = points + pageWidthHalf / 10;
                player.charged_attack = false;
            }
            else {
                enemy.charged_attack = true;
                points = points + pageWidthHalf / 20;
            }
            var myAudio = document.getElementById("punch-defended");
            myAudio.play();
            move_image(points, player);
        }
    }
}

const attack_action2 = (player, enemy) => {

    if (player.defense === true) {
        console.log("no puedes atacar mientras defiendes");
    }
    else {
        if (enemy.defense === false) {
            enemy.hp--;
            if (player.charged_attack === true) {
                points = points - pageWidthHalf / 5;
                var myAudio = document.getElementById("ping");
                myAudio.play();
                player.charged_attack = false;
            }
            else {
                var myAudio = document.getElementById("punch");
                myAudio.play();
                points = points - pageWidthHalf / 10;
            }
            move_image(points, player);
        }
        else {
            console.log("defendido");
            if (player.charged_attack === true) {
                points = points - pageWidthHalf / 10;
                player.charged_attack = false;
            }
            else {
                enemy.charged_attack = true;
                points = points - pageWidthHalf / 20;
            }
            var myAudio = document.getElementById("punch-defended");
            myAudio.play();
            move_image(points, player);
        }
    }
}
let player1 = new Player('p1', 10, attack_action1, false, false);
let player2 = new Player('p2', 10, attack_action2, false, false);


const atackplayer1throttled = throttle(500, player1.attack);
const atackplayer2throttled = throttle(500, player2.attack);
const resetGamethrottled = throttle(1500, clearScore);

this.addEventListener('keydown', event => {
    if (event.key == 'a') {
        buttona.style.backgroundColor = "red";
        atackplayer1throttled(player1, player2);
        console.log(points);
    }

    if (event.key == 'k') {
        buttonk.style.backgroundColor = "red";
        atackplayer2throttled(player2, player1);
        console.log(points);
    }

    if (event.key == 's') {
        buttons.style.backgroundColor = "green";
        player1.defense = true;

    }

    if (event.key == 'l') {
        buttonl.style.backgroundColor = "green";
        player2.defense = true;
    }
})

this.addEventListener('keyup', event => {
    if (event.key == 'a') {
        buttona.style.backgroundColor = "";
    }

    if (event.key == 'k') {
        buttonk.style.backgroundColor = "";
    }

    if (event.key == 's') {
        buttons.style.backgroundColor = "";
        player1.defense = false;
    }

    if (event.key == 'l') {
        buttonl.style.backgroundColor = "";
        player2.defense = false;
    }
})

buttonresetgame.addEventListener('click', event => {
    clearScore();
    cleanGame();
})

window.onload = function () {
    var resize = pageWidthHalf + points;
    console.log(pageWidthHalf, points);
    move_image(points);
    //document.getElementById("images").style.marginLeft = resize + "px";
};