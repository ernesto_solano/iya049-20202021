const createServer = require("./create_server.js");
const fs = require('fs');       //api nodejs https://nodejs.org/api/fs.html 
const path = require('path');   //api nodejs https://nodejs.org/api/path.html

const get = (request, response) => {
  let filePath = '.' + request.path;
  if (filePath == './')
    filePath = './index.html';

  var extName = path.extname(filePath);
  var contentType = 'text/html';
  switch (extName) {            //https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.ico':
      contentType = 'image/x-icon';
      break;
    case '.png':
      contentType = 'image/png';
      break;
    case '.jpg':
      contentType = 'image/jpeg';
      break;
    case '.gif':
      contentType = 'image/gif';
      break;
    case '.json':
      contentType = 'application/json';
      break;
    case '.mp3':
      contentType = 'audio/mpeg';
      break;
    case '.ogg':
      contentType = 'audio/ogg';
      break;
  }

  fs.readFile(filePath, function (error, file) {
    if (error) {
      response.send(
        "500",
        { "Content-Type": "text/html" },
        `<html><body>${error}</body></html>`
      );
    }
    else {
      response.send(
        "200",
        {
          "Content-Type": contentType,
        },
        file
      );
    }
  });
};

const post = (request, response) => {
  console.log(request.body);
  let winnerRequest = JSON.parse(request.body)["winner"];
  if (request.path == '/score') {
    if (!fs.existsSync('score.json')) {
      initialData = { "p1": 0, "p2": 0 }
      fs.writeFile('score.json', JSON.stringify(initialData), 'utf8', (err) => {
        if (err) throw err;
        console.log("creado");
      });
    }
    fs.readFile('score.json', function (err, data) {
      if (err) throw err;

      let json = JSON.parse(data);
      console.log(winnerRequest)
      if (winnerRequest == "p1") {
        json["p1"]++;
      }
      if (winnerRequest == "p2") {
        json["p2"]++;
      }
      fs.writeFile('score.json', JSON.stringify(json), 'utf8', (err) => {
        if (err) throw err;
        console.log("agregado");
      });
    })
    response.send(
      200,
      {
        "Content-Type": "application/json",
      },
      request.body
    );
    /* fs.appendFile('score.json', request.body, function (err) {
      if (err) throw err;
      response.send(
        200,
        {
          "Content-Type": "application/json",
        },
        request.body

      );
    }); */
  }
  else if (request.path == '/score/clear') {
    initialData = { "p1": 0, "p2": 0 }
    fs.writeFile('score.json', JSON.stringify(initialData), 'utf8', (err) => {
      if (err) throw err;
      console.log("creado");
    });
    response.send(
      200,
      {
        "Content-Type": "application/json",
      },
      request.body
    );
  }

  else {
    response.send(
      "404",
      { "Content-Type": "text/html" },
      `<html><body> De esa no le tenemos </body></html>`
    );
  }

};

const requestListener = (request, response) => {
  switch (request.method) {
    case "GET": {
      return get(request, response);
    }
    case "POST": {
      return post(request, response);
    }
    default: {
      return response.send(
        404,
        { "Content-Type": "text/plain" },
        "The server only supports HTTP methods GET and POST"
      );
    }
  }
};

const server = createServer((request, response) => {
  try {
    return requestListener(request, response);
  } catch (error) {
    console.error(error);
    response.send(500, { "Content-Type": "text/plain" }, "Uncaught error");
  }
});

server.listen(8080);
